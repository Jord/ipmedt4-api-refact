<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meldingen;

class MeldingenController extends Controller
{
    public function showStationsMeldingen($station){
        $meldingen = Meldingen::where("stations_id", "=", $station)->get();

        return response()->json([
          'meldingen' => $meldingen,
        ]);
      }

    public function showMeldingen(){
        $meldingen = Meldingen::get();
        foreach($meldingen as $melding){
          $apiUrl = url()->current() . "/api/meldingen/station/" . $melding->stations_id;
          $melding->station = $apiUrl;
        }
        return response()->json([
          'meldingen' => $meldingen,
        ]);
      }
}
