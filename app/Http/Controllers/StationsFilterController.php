<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;
use App\Station;
use App\FilterCategorieen;
use Illuminate\Database\Eloquent\Collection;

class StationsFilterController extends Controller
{
    public function show(){
        $stationTable = Station::get();
        $filter = Filter::get();


        return response()->json([
          'stations' => $station,
          'filters' => $filter,
        ]);
      }

      /**
      * Takes an array and returns an array of duplicate items
      */
      public function get_duplicates( $array ) {
          return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
      }

      public function showStationFilters($station){
        $getStation = Station::where('stationsnaam', '=', $station)->get();
        if(isset($getStation[0]->id)){
          $filtersWithDupes = Filter::join('station_filters', function ($join) use ($station, $getStation) {
              $join->on("id", '=', 'station_filters.filters_id')
                   ->where('station_filters.stations_id', '=', $getStation[0]->id);
          })
          ->get();
        } else {
          $filtersWithDupes = Filter::join('station_filters', function ($join) use ($station, $getStation) {
              $join->on("id", '=', 'station_filters.filters_id')
                   ->where('station_filters.stations_id', '=', $station);
          })
          ->get();
        }
        $groupedByFiltersId = $filtersWithDupes->groupBy('naam');
        $filters = $groupedByFiltersId->filter(function ( Collection $groups ) {
          return $groups->count();
        });

        $categorieen = Filter::select('categorie')->groupBy('categorie')->get();

        foreach($categorieen as $categorie){
          $categorie->checked = false;
        }


        return response()->json([
          'filterCategorieen' => $categorieen,
          'filters' => $filters,
        ]);
      }
}
