<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->unsignedInteger('id')->unique();
            $table->string("naam");
            $table->text("imageUrl");
            $table->string("categorie");
            $table->foreign('categorie')
            ->references('naam')->on('filter_categorieen')
            ->onUpdate('cascade')
            ->onDelete("cascade");
            $table->string("backgroundColor");
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
