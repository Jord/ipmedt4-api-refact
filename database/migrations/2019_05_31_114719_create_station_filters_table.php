<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_filters', function (Blueprint $table) {
            $table->unsignedInteger('stations_id');
            $table->unsignedInteger('filters_id');
            $table->text('beschrijving');
            $table->text('tijden');
            $table->double('latitude');
            $table->double('longitude');
        });

        Schema::table('station_filters', function (Blueprint $table){
            $table->foreign("stations_id","filters_id")
            ->references("id","id")
            ->on("stations","filters")
            ->onDelete("cascade","cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_filters');
    }
}
