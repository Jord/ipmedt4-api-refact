<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeldingen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meldingen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('stations_id');
            $table->string("image");
            $table->string("titel");
            $table->string("beschrijving");
            $table->text("tekst");
            $table->timestamps();
        });

        Schema::table('meldingen', function (Blueprint $table){
            $table->foreign("stations_id")
            ->references("id")
            ->on("stations")
            ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meldingen');
    }
}
