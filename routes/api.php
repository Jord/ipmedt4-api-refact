<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/stations','StationsController@show');

Route::get('/meldingen/station/{station}','MeldingenController@showStationsMeldingen');
Route::get('/meldingen','MeldingenController@showMeldingen');

Route::get('/filters/{station}','StationsFilterController@showStationFilters');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
